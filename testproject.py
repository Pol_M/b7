from pathlib import Path
import pathlib
import os
from io import IOBase
from random import choice
from primelist import primes
class ProjFile:
	def __init__(self, filePath):
		self.source = Path(str(filePath) + ".cc")
		self.header = Path(str(filePath) + ".hh")
		self.object = Path(str(filePath) + ".o")

class TestProject:
	def __init__(self, objects, dir):
		parent = Path(dir)
		self.isolatedFolder = parent / str("dir.{0}.test".format(objects))
		if self.isolatedFolder.exists():
			raise Exception("path \"{0}\" already exists".format(self.isolatedFolder))
		os.mkdir(self.isolatedFolder)
		self.files = [ProjFile(self.isolatedFolder / ("fi{0}".format(i))) for i in range(objects)]
		for index, path in enumerate(self.files):
			with open(path.source,"w+") as f:
				self.createSource(f, index)
			with open(path.header,"w+") as f:
				self.createHeader(f, index)
		main = ProjFile(self.isolatedFolder / ("main"))
		with open(main.source,"w+") as f:
			self.createMain(f)
		self.files.append(main)

	def createSource(self, file : IOBase, num):
		for path in self.files:
			file.write("#include \"{0}\"\n".format(path.header))
		file.write("int use{0}(int p){{int t = 2;\n".format(num))
		for index in range(len(self.files)):
			if index != num:
				for i in range(512):
					file.write("\tt = f{0}_{1}(t);\n".format(index, i))
		file.write("\treturn t;}\n")
		for i in range(512):
			file.write(
				"int f{0}_{1}(int t){{return t{2}{3};}}\n".format(num, i, choice(["+","-","*","/"]), primes[i]))

	def createHeader(self, file : IOBase, num):
		file.write("#pragma once\n")
		file.write("int use{0}(int p);\n".format(num))
		for i in range(512):
			file.write("int f{0}_{1}(int t);\n".format(num, i))

	def createMain(self, file : IOBase):
		for path in self.files:
			file.write("#include \"{0}\"\n".format(path.header))
		file.write("int main(){int t = 2;\n")

		for index in range(len(self.files)):
			file.write("\tt = use{0}(t);\n".format(index))
		file.write("\treturn t;}\n")

if __name__ == "__main__":
	pass
