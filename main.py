import sys
from testproject import TestProject
from random import shuffle
from compile import Compile, Clang, GCC

def main():
	if len(sys.argv) != 5:
		print("Please provide a size objects, size objects max, size objects step, repeats, threads of the machine and path. Given: ", sys.argv)
		return
	numObjectsMin = int(sys.argv[1])
	numObjectsMax = int(sys.argv[2])
	numObjectsStep = int(sys.argv[3])
	repeats = int(sys.argv[4])
	numThreads = int(sys.argv[5])
	dir = str(sys.argv[6])

	while numObjectsMax > numObjectsMin:
		test = TestProject(numObjectsMin, dir)
		compilers = [Clang(test), GCC(test)]
		shuffle(compilers)
		timeRes = []
		for compiler in compilers:
			compiler.compile(numThreads)
			print(compiler.compiler(), [compiler.link() for i in range(repeats)])
		numObjectsMin += numObjectsStep

if __name__ == "__main__":
	main()