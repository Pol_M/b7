from testproject import TestProject
from multiprocessing import Pool
from os import system
import datetime

class Compile:
    def __init__(self, files : TestProject):
        self.files = files.files
        self.isolatedFolder = files.isolatedFolder

    def compiler(self):
        raise Exception("base class Compile")

    def linker(self):
        raise Exception("base class Compile")

    def compile(self, threads):
        with Pool(threads) as p:
            p.map(system,
                ["{0} -c -o {1} {2}".format(self.compiler(), file.object, file.source) for file in self.files])

    def link(self):
        command = "{0} -o {1} {2} {3}".format(self.compiler(),self.isolatedFolder / "main.exe", ' '.join([str(i.object) for i in self.files]), self.linker())
        a = datetime.datetime.now()
        system(command)
        b = datetime.datetime.now()
        return (b-a).total_seconds()

class GCC(Compile):
    def __init__(self, files : TestProject):
        super().__init__(files)

    def compiler(self):
        return "gcc"

    def linker(self):
        return ""

class Clang(Compile):
    def __init__(self, files : TestProject):
        super().__init__(files)

    def compiler(self):
        return "clang"

    def linker(self):
        return "-fuse-ld=lld"
